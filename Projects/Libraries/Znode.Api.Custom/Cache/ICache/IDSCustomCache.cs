﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Cache;

namespace Znode.Api.Custom.Cache.ICache
{
    public interface IDSCustomCache
    {
        //string GetInventoyGrid(int PublishProductId, string color);
        string GetInventoyGrid(int PublishProductId, string Color, string RouteUri, string RouteTemplate);

    }
}
