﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    interface IDSCustomAgent
    {
        //string GetInventoyGrid(int PublishProductId, string Color);
        DSCustomListViewModel GetInventoyGrid(int PublishProductId, string Color);
    }
}
